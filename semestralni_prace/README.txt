Semestral work topic: 7 - Kaggle competetion
Choose an open Kaggle competetion from the Featured or Research categories and create
your own solution.

Since I am a typical student who leaves their work for the absolutly last possible moment,
I got locked out of my chosen competetion. I did not know this could happen as I used
data from Kaggle before and closed competetions had data available. In order to hand in
the work on time, I had to choose a different competetion, link here:
https://www.kaggle.com/competitions/learning-equality-curriculum-recommendations

The goal is to recommend learning content based on a topic. Submissions are evaluated
based on their mean F2 score.

All used data is stored in the GITLAB repository. The code is stored in the .ipynb format
and can be run in Jupyter Notebook or Google Collab if needed.

Dependencies:
pandas==1.2.3
numpy==1.19.5
seaborn==0.11.1
torch==1.13.1+cu117
transformers==4.25.1

